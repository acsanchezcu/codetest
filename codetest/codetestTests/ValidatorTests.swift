//
//  ValidatorTests.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import XCTest

class ValidatorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testValidInput() {
        let values = ["Michael Jackson", " Michael", "Michael ", "balón", "BALÓN", "Michael  Jackson", "Jackson 5", "13"]
        
        for value in values {
            XCTAssertTrue(Validator.stringIsAlphanumeric(value))
        }
    }
    
    func testNotValidInput() {
        let values = ["example,", "example;", "it's not valid", "example🤗"]
        
        for value in values {
            XCTAssertFalse(Validator.stringIsAlphanumeric(value))
        }
    }
    
}
