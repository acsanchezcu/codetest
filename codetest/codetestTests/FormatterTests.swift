//
//  Formatter.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 2/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import XCTest

class FormatterTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFormatter() {
        let values = [" Michael Jackson ", "\nMichael Jackson", "Michael Jackson ", "Michael  Jackson", "  Michael Jackson", "Michael Jackson  "]
        
        for value in values {
            XCTAssertTrue(Formatter.formatStringToRequest(string: value) == "Michael+Jackson")
        }
    }
    
}
