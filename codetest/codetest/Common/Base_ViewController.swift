//
//  Base_ViewController.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 2/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Base_ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap_gesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
        
        tap_gesture.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap_gesture)
    }
    
    func hideKeyboard(_ sender: Any) {
        splitViewController?.view.endEditing(true)
    }

}
