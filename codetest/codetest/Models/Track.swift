//
//  Track.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Track {
    let artistName: String
    let trackName: String
    let artworkUrl: String
    let albumName: String
    let price: Double
    let releaseDate: String
    let trackId: String
    
    init(artistName: String, trackName: String, artworkUrl: String, albumName: String, price: Double, releaseDate: String, trackId: String) {
        self.artistName = artistName
        self.trackName = trackName
        self.artworkUrl = artworkUrl
        self.albumName = albumName
        self.price = price
        self.releaseDate = releaseDate
        self.trackId = trackId
    }
}
