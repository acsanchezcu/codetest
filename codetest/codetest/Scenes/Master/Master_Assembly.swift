//
//  Master_Assembly.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//


import UIKit

class Master_Assembly: NSObject {
    
    static let sharedInstance = Master_Assembly()
    
    func configure(_ viewController: Master_ViewController) {
        let interactor = Master_Interactor()
        let presenter = Master_Presenter()
        let router = Master_Router()
        
        viewController.presenter = presenter
        
        presenter.viewController = viewController
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        interactor.router = router
        
        router.viewController = viewController
    }
    
}
