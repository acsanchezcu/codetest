//
//  Master_Track_ViewModel.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

struct Master_Track_ViewModel {
    
    let artist: String
    let track: String
    let thumbnailUrl: String
    let trackId: String
    
}
