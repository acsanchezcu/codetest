//
//  Master_Presenter.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Master_Presenter: NSObject {
    
    var viewController: Master_ViewController_Protocol!
    var interactor: Master_Interactor_Protocol!
    
}

extension Master_Presenter: Master_Presenter_Protocol {
    
    func presentError(_ error: Error) {
        viewController.displayAlerWithTitle("Error", message: error.localizedDescription)
    }
    
    func presentTracks(_ tracks: [Track]) {
        let view_models = tracks.map { (track) -> Master_Track_ViewModel in
            let view_model = Master_Track_ViewModel.init(artist: track.artistName, track: track.trackName, thumbnailUrl: track.artworkUrl, trackId: track.trackId )
            
            return view_model
        }

        viewController.viewModels = view_models
        
        if view_models.count == 0 {
            viewController.displayAlerWithTitle("Info", message: "No results for \(viewController.searchTextField.text!)")
        }
    }
    
    func didSelectTrackWithId(_ trackId: String) {
        interactor.showDetailOfTrackId(trackId)
    }
    
    func displayLoading() {
        viewController.displayLoading()
    }
    
    func dismissLoading() {
        viewController.dismissLoading()
    }
    
}

extension Master_Presenter: UITextFieldDelegate {
    
    //Check is the textField is valid to added it
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let valid_alphanumeric_string = Validator.stringIsAlphanumeric(string)
        
        return valid_alphanumeric_string
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        viewController.viewModels = []
        
        if (textField.text?.characters.count)! > 0 {
            var search_formatted = Formatter.formatStringToRequest(string: textField.text!)
            
            //url allowed string
            search_formatted = search_formatted.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
            
            interactor.fetchTracks(search: search_formatted)
        }
        
        return true
    }
    
}
