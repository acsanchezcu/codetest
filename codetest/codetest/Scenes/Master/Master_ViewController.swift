//
//  Master_ViewController.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Master_ViewController: Base_ViewController {
    
    // MARK: - Properties
    var presenter: Master_Presenter_Protocol!
    var loadingView: Loading_View?
    
    private var _viewModels: [Master_Track_ViewModel]! = []
    var viewModels: [Master_Track_ViewModel] {
        get {
            return _viewModels
        }
        set {
            _viewModels = newValue
            
            OperationQueue.main.addOperation({
                if newValue.count > 0 {
                    self.noResultsLabel.isHidden = true
                } else {
                    self.noResultsLabel.isHidden = false
                }
                
                self.tableView.reloadData()
            })
        }
    }
    
    // MARK: - Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noResultsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Master_Assembly.sharedInstance.configure(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView.init()
        
        let cell_name = String(describing: Master_Track_TableViewCell.self)
        
        tableView.register(UINib.init(nibName: cell_name, bundle: nil), forCellReuseIdentifier: cell_name)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = Master_Track_TableViewCell.estimatedHeight()
        
        searchTextField.delegate = presenter
        noResultsLabel.text = "No results, try to search something!"
        title = "Tracks"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension Master_ViewController: Master_ViewController_Protocol {
    
    func displayAlerWithTitle(_ title: String, message: String) {
        let alert_controller = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        let ok_action = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        
        ok_action.setValue(UIColor.init(hex: 0x800000), forKey: "titleTextColor")
        
        alert_controller.addAction(ok_action)
        
        present(alert_controller, animated: true, completion: nil)
    }
    
    func displayLoading() {
        loadingView = Loading_View.init()
        
        splitViewController?.view.addSubview(loadingView!)
        
        UIView.embedToSuperView(loadingView!)
    }
    
    func dismissLoading() {
        DispatchQueue.main.async() { () -> Void in
            self.loadingView?.removeFromSuperview()
        }
    }
    
}

extension Master_ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell_reuse_identifier = String(describing: Master_Track_TableViewCell.self)
        
        let cell: Master_Track_TableViewCell = tableView.dequeueReusableCell(withIdentifier: cell_reuse_identifier, for: indexPath) as! Master_Track_TableViewCell
        
        let view_model = viewModels[indexPath.row]
        
        cell.artistNameLabel.text = view_model.artist
        cell.trackNameLabel.text = view_model.track
        cell.thumbnailImageView.image = #imageLiteral(resourceName: "placeholder_image")
        
        cell.thumbnailImageView.imageFromUrl(view_model.thumbnailUrl)
        
        return cell
    }
}

extension Master_ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let view_model = viewModels[indexPath.row]
        
        presenter.didSelectTrackWithId(view_model.trackId)
    }
    
}



