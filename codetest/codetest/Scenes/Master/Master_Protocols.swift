//
//  Master_Protocols.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

protocol Master_ViewController_Protocol {
    var presenter: Master_Presenter_Protocol! { get }
    var viewModels: [Master_Track_ViewModel] { get set }
    
    weak var searchTextField: UITextField! { get }
    
    func displayLoading()
    func dismissLoading()
    func displayAlerWithTitle(_ title: String, message: String)
}

protocol Master_Presenter_Protocol: UITextFieldDelegate {
    var viewController: Master_ViewController_Protocol! { get }
    var interactor: Master_Interactor_Protocol! { get }
    
    func displayLoading()
    func dismissLoading()
    func presentTracks(_ tracks: [Track])
    func presentError(_ error: Error)
    func didSelectTrackWithId(_ trackId: String)
}

protocol Master_Interactor_Protocol {
    var presenter: Master_Presenter_Protocol! { get }
    var router: Master_Router_Protocol! { get }
    
    func fetchTracks(search: String)
    func showDetailOfTrackId(_ trackId: String)
}

protocol Master_Router_Protocol {
    var viewController: Master_ViewController! { get }
    
    func showDetailOfTrack(_ track: Track)
}
