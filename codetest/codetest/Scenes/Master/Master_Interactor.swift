//
//  Master_Interactor.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Master_Interactor: NSObject {
    
    var presenter: Master_Presenter_Protocol!
    var router: Master_Router_Protocol!
    
    var tracks: [Track] = []
    
}

extension Master_Interactor: Master_Interactor_Protocol {
    
    func fetchTracks(search: String) {
        var request = URLRequest(url: URL(string: "https://itunes.apple.com/search?term=\(search)")!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        
        presenter.displayLoading()
        
        session.dataTask(with: request) {data, response, error in
            self.presenter.dismissLoading()
            
            do{
                if (error != nil) {
                    self.presenter.presentError(error!)
                    
                    return
                }
                
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                
                let results: [AnyObject] = json["results"] as! [AnyObject]
                
                self.tracks = []
                
                for result in results {
                    let artist_name = result["artistName"] as? String ?? ""
                    let track_name = result["trackName"] as? String ?? ""
                    var artwork_url = result["artworkUrl100"] as? String ?? ""
                    let album_name = result["collectionName"] as? String ?? ""
                    let price_track = result["trackPrice"] as? Double ?? 0
                    let release_date = result["releaseDate"] as? String ?? ""
                    let track_id = result["trackId"] as? Int ?? 0
                    
                    if artwork_url.characters.count > 0 {
                        //to improve quality of image
                        artwork_url = artwork_url.replacingOccurrences(of: "100x100", with: "500x500")
                    }
                    
                    let track: Track = Track.init(artistName: artist_name, trackName: track_name, artworkUrl: artwork_url, albumName: album_name, price: price_track, releaseDate: release_date, trackId: String(track_id))
                    
                    self.tracks.append(track)
                }
                
                self.presenter.presentTracks(self.tracks)
            }catch let error as NSError{
                self.presenter.presentError(error)
            }
            
            }.resume()
    }
    
    func showDetailOfTrackId(_ trackId: String) {
        
        let filtered_tracks = tracks.filter { $0.trackId == trackId
        }
        
        if let track = filtered_tracks.first {
            router.showDetailOfTrack(track)
        }
    }
    
}
