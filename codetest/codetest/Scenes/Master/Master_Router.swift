//
//  Master_Router.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Master_Router: NSObject {
    var viewController: Master_ViewController!
}

extension Master_Router: Master_Router_Protocol {
    
    func showDetailOfTrack(_ track: Track) {
        if let split = viewController.splitViewController {
            let controllers = split.viewControllers
            if let detail_viewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? Detail_ViewController {
                let detail_presenter: Detail_Presenter_Protocol = detail_viewController.presenter
                let detail_interactor: Detail_Interactor_Protocol = detail_presenter.interactor
                
                detail_interactor.displayDetailTrack(track)
            } else {
                let detail_viewController = Detail_Assembly.sharedInstance.configure()
            
                let presenter = detail_viewController.presenter
                var interactor = presenter?.interactor
                
                interactor?.track = track
                
                viewController.navigationController?.pushViewController(detail_viewController.navigationController!, animated: true)
            }
        }
    }
    
}
