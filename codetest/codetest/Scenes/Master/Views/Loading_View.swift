//
//  Loading_View.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 2/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Loading_View: UIView {
    
    var view: UIView?

    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view?.frame = bounds
        
        // Make the view stretch with containing view
        view?.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view!)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        
        let nib = UINib(nibName: String(describing: Loading_View.self), bundle: bundle)
        
        // Assumes UIView is top level and only object in Loading_View.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
