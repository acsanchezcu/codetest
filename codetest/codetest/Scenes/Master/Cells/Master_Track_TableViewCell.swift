//
//  Master_Track_TableViewCell.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Master_Track_TableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var trackNameLabel: UILabel!
    
    // MARK: - Methods

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Class Functions

    class func estimatedHeight() -> CGFloat {
        return 45.0
    }
}
