//
//  Detail_Interactor.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Detail_Interactor: NSObject {
    
    var presenter: Detail_Presenter_Protocol!
    var router: Detail_Router_Protocol!
    
    private var _track: Track?
    var track: Track? {
        get {
            return _track
        }
        set {
            _track = newValue
        }
    }
}

extension Detail_Interactor: Detail_Interactor_Protocol {
    
    func displayDetailTrack(_ track_value: Track) {
        track = track_value
        
        presenter.presentDetailTrack(track!)
    }
    
}
