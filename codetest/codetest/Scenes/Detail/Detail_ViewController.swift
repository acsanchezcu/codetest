//
//  Detail_ViewController.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Detail_ViewController: Base_ViewController {
    
    var presenter: Detail_Presenter_Protocol!
    
    private var _viewModel: Detail_Track_ViewModel?
    var viewModel: Detail_Track_ViewModel? {
        get {
            return _viewModel
        }
        set {
            _viewModel = newValue
            
            if let view_model = viewModel {
                if view_model.album.characters.count > 0 {
                    albumLabel.text = view_model.album
                } else {
                    albumStackView.isHidden = true
                }
                
                if view_model.track.characters.count > 0 {
                    trackLabel.text = view_model.track
                } else {
                    trackStackView.isHidden = true
                }
                
                if view_model.price.characters.count > 0 {
                    priceLabel.text = view_model.price
                } else {
                    priceStackView.isHidden = true
                }
                
                if view_model.releaseDate.characters.count > 0 {
                    releaseDateLabel.text = view_model.releaseDate
                } else {
                    releaseDateStackView.isHidden = true
                }
                
                if view_model.artist.characters.count > 0 {
                    artistLabel.text = view_model.artist
                } else {
                    artistStackView.isHidden = true
                }
                
                thumbnailImageView.image = #imageLiteral(resourceName: "placeholder_image")
                thumbnailImageView.imageFromUrl(view_model.thumbnailUrl)
                
                detailStackView.isHidden = false
                noDetailLabel.isHidden = true
            } else {
                detailStackView.isHidden = true
                noDetailLabel.isHidden = false
            }
        }
    }
    
    var viperConfigured: Bool = false
    
    // MARK: - Outlets
    
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var albumStackView: UIStackView!
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var trackStackView: UIStackView!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var artistStackView: UIStackView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceStackView: UIStackView!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var releaseDateStackView: UIStackView!
    
    @IBOutlet weak var detailStackView: UIStackView!
    @IBOutlet weak var noDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !viperConfigured {
            Detail_Assembly.sharedInstance.configure(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailStackView.isHidden = true
        noDetailLabel.isHidden = false
        
        presenter.viewIsReady()
        
        thumbnailImageView.layer.cornerRadius = UIDevice.current.userInterfaceIdiom == .pad ? 100.0 : 50.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
}

extension Detail_ViewController: Detail_ViewController_Protocol {
    
}
