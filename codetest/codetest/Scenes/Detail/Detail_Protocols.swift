//
//  Detail_Protocols.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

protocol Detail_ViewController_Protocol {
    var presenter: Detail_Presenter_Protocol! { get }
    
    var viewModel: Detail_Track_ViewModel? { get set }
}

protocol Detail_Presenter_Protocol {
    var viewController: Detail_ViewController_Protocol! { get }
    var interactor: Detail_Interactor_Protocol! { get }
    
    func viewIsReady()
    func presentDetailTrack(_ track: Track)
}

protocol Detail_Interactor_Protocol {
    var presenter: Detail_Presenter_Protocol! { get }
    var router: Detail_Router_Protocol! { get }
    
    var track: Track? { get set }
    func displayDetailTrack(_ track: Track)
}

protocol Detail_Router_Protocol {
    var viewController: Detail_ViewController! { get }
}
