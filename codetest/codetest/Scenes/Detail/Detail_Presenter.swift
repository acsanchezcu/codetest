//
//  Detail_Presenter.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

class Detail_Presenter: NSObject {
    
    var viewController: Detail_ViewController_Protocol!
    var interactor: Detail_Interactor_Protocol!
    
}

extension Detail_Presenter: Detail_Presenter_Protocol {
    
    func presentDetailTrack(_ track: Track) {
        let view_model = Detail_Track_MapperViewModel.mapperTrack(track)
        
        viewController.viewModel = view_model
    }
    
    func viewIsReady() {
        if let track = interactor.track {
            presentDetailTrack(track)
        }
    }
    
}
