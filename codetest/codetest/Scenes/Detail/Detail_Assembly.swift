//
//  Detail_Assembly.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright (c) 2017 Abel Sánchez Custodio. All rights reserved.
//


import UIKit

class Detail_Assembly: NSObject {
    
    static let sharedInstance = Detail_Assembly()
    
    //Configuration in awakeFromNib for iPad 7 & Plus devices in landscape
    
    func configure(_ viewController: Detail_ViewController) {
        let interactor = Detail_Interactor()
        let presenter = Detail_Presenter()
        let router = Detail_Router()
        
        viewController.presenter = presenter
        
        presenter.viewController = viewController
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        interactor.router = router
        
        router.viewController = viewController
        
        viewController.viperConfigured = true
    }
    
    func configure() -> Detail_ViewController {
        let interactor = Detail_Interactor()
        let presenter = Detail_Presenter()
        let router = Detail_Router()
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let identifier = String(describing: Detail_ViewController.self)
        let navigation_controller = storyboard.instantiateViewController(withIdentifier: identifier) as! UINavigationController
        let view_controller = navigation_controller.topViewController as! Detail_ViewController
        
        view_controller.presenter = presenter
        
        presenter.viewController = view_controller
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        interactor.router = router
        
        router.viewController = view_controller
        
        view_controller.viperConfigured = true
        
        return view_controller
    }
    
}
