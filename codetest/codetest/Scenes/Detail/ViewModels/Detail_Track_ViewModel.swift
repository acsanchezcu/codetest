//
//  Detail_Track_ViewModel.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

struct Detail_Track_ViewModel {

    let album: String
    let track: String
    let thumbnailUrl: String
    let price: String
    let releaseDate: String
    let artist: String
    
}

class Detail_Track_MapperViewModel {
    
    class func mapperTrack(_ track: Track) -> Detail_Track_ViewModel {
        let price = "\(String(track.price))$"
        
        let date_formatter = DateFormatter()
        
        date_formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SSZ"
        
        var date_string = track.releaseDate
        
        if let date = date_formatter.date(from: track.releaseDate) {
            date_formatter.dateFormat = "MMMM dd, yyyy"
            
            date_string = date_formatter.string(from: date)
        }
        
        let view_model = Detail_Track_ViewModel.init(album: track.albumName, track: track.trackName, thumbnailUrl: track.artworkUrl, price: price, releaseDate: date_string, artist: track.artistName)
        
        return view_model
    }
    
}
