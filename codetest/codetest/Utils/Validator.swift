//
//  Validator.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 1/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import Foundation

class Validator {
    
    class func stringIsAlphanumeric(_ string: String) -> Bool {
        return string.range(of: "[^a-zA-Z0-9 á-úÁ-Ú]", options: .regularExpression) == nil
    }
    
}
