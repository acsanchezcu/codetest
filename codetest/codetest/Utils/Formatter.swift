//
//  Formatter.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 2/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import Foundation

class Formatter {
    
    class func formatStringToRequest(string: String) -> String {
        //remove extra whitespaces
        var final_string = string.replacingOccurrences(of: "  ", with: " ")
        
        //trimming whitespaces/new lines
        final_string = final_string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        return final_string.replacingOccurrences(of: " ", with: "+")
    }
    
}
