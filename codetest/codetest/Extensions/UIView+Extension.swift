//
//  UIView+Extension.swift
//  codetest
//
//  Created by Abel Sánchez Custodio on 2/4/17.
//  Copyright © 2017 Abel Sánchez Custodio. All rights reserved.
//

import UIKit

extension UIView {
    
    class func embedToSuperView(_ view: UIView) {
        let views = ["view": view]
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let width_constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        let height_constraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        view.superview!.addConstraints(width_constraints)
        view.superview!.addConstraints(height_constraints)
    }
    
}
