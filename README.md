# Codetest

2 April, 2017 v1.0

## Requirements

- iOS 9.0+
- Xcode 8.1+
- Swift 3.0+
- Supports iPhone/iPad landscape/portrait

## Info

Search a list of tracks with itunes API. You can check details as name of the album, artist, year of release, prices, etc.

## Architecture

Application is a VIPER-based build architecture. There are two scenes, Master & Detail, each one with their VIPER classes. Master will contain a result list and Detail will show more details about each of the results.

More info about [VIPER](https://www.objc.io/issues/13-architecture/viper/)

Application uses a Master-Detail design pattern.

## TODO in future

- Segmentation based on a colors palette. It'll have value over headers label, navigationBar tint colors, alert tint colors, layer of some views, lines separator between views, etc.
- More unit tests for the different scenes.
- A cache system for the thumbnail albums.
- Expand information in detail.
- Link urls to Safari browser.
- Design for launch screen, icons app, custom alerts...

## Bitacora

- The main problem of project was the integration between the complex VIPER architecture and the Master-Detail design pattern. Especially devices as iPhones Plus  that have both designs, one for portrait and other for landscape. It was basic to know when it was really neccesary to create a new instance of the Detail scene (via Assembly)